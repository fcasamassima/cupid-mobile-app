/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.cupid.logging_app;

//import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Arrays;

import com.androidplot.xy.*;
//import com.androidplot.series.XYSeries;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.SimpleXYSeries;
import eu.cupid.logging_app.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * This is the main Activity that displays the current chat session.
 */
public class CupidLogApp extends Activity {
    // Debugging
    private static final String TAG = "BluetoothChat";
    private static final boolean D = true;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // Layout Views
    private TextView mTitle;
    private ListView mConversationView;
    private Button mSendStartButton;
    private Button mSendStopButton;
    private Button mStartLogButton;
    private EditText mPatientID;
    private EditText mTrialID;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // String buffer for outgoing messages
   // private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothService[] mChatService = new BluetoothService[7];
    
    private String[] ConnectedDevicesNames = new String[7];
    
    public String dirName, dirName2,update_name_log,file_name_log;
    File root;
    //private int day, month, year,l_num;
    private File log_file;
    private FileWriter log_file_wr;
    private BufferedWriter out_log;
    
  //  public long StartStremingTime;
    private XYPlot aprLevelsPlot = null;
    private SimpleXYSeries aprLevelsSeries = null;
    private Number[] SensorsData= {-16384,-16384,-16384,-16384,-16384,-16384,-16384};
    
    private int ConnectedDevices = 0;
    private Time now = new Time(); 
    {
    	aprLevelsSeries = new SimpleXYSeries("APR Levels");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");

        // Set up the window layout
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        // Set up the custom title
        mTitle = (TextView) findViewById(R.id.title_left_text);
        mTitle.setText(R.string.app_name);
        mTitle = (TextView) findViewById(R.id.title_right_text);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        
        // setup the APR Levels plot:
       aprLevelsPlot = (XYPlot) findViewById(R.id.aprLevelsPlot);
 //      aprLevelsPlot.addSeries(aprLevelsSeries, new BarFormatter(Color.argb(100, 0, 200, 0), Color.rgb(0, 80, 0)));
       aprLevelsPlot.addSeries(aprLevelsSeries, BarRenderer.class, new BarFormatter(Color.argb(100, 0, 200, 0), Color.rgb(0, 80, 0)));
        aprLevelsPlot.setDomainStepValue(3);
        aprLevelsPlot.setTicksPerRangeLabel(3);

        // per the android documentation, the minimum and maximum readings we can get from
        // any of the orientation sensors is -180 and 359 respectively so we will fix our plot's
        // boundaries to those values.  If we did not do this, the plot would auto-range which
        // can be visually confusing in the case of dynamic plots.
        

        // use our custom domain value formatter:
       // aprLevelsPlot.setDomainValueFormat(new APRIndexFormat());

        // update our domain and range axis labels:
        aprLevelsPlot.setDomainLabel("Sensor");
        aprLevelsPlot.getDomainLabelWidget().pack();
        aprLevelsPlot.setRangeLabel("Acceleration");
        aprLevelsPlot.getRangeLabelWidget().pack();

        aprLevelsPlot.setGridPadding(10, 0, 10, 0);
        aprLevelsPlot.disableAllMarkup();
        
        // get a ref to the BarRenderer so we can make some changes to it:
        BarRenderer barRenderer = (BarRenderer) aprLevelsPlot.getRenderer(BarRenderer.class);
        if(barRenderer != null) {
            // make our bars a little thicker than the default so they can be seen better:
            barRenderer.setBarWidth(25);
        }
        
        
        
        
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        
        
        aprLevelsPlot.setRangeBoundaries(-16384, 16384, BoundaryMode.FIXED);
        aprLevelsSeries.setModel(Arrays.asList(SensorsData), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
        aprLevelsPlot.redraw();
        
        for (int i = 0; i < 7; i++) {
        	ConnectedDevicesNames[i] = "";
        }
    }
	


    @Override
    public void onStart() {
        super.onStart();
        mTrialID = (EditText)findViewById(R.id.editTextTrial);
        mPatientID = (EditText)findViewById(R.id.editTextPatient);
        mTrialID.setText("0");
        mPatientID.setText("0");
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if(D) 
        	Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the chat session
        } else {
            if (mChatService[0] == null) 
            	setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService[0] != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService[0].getState() == BluetoothService.STATE_NONE) {
              // Start the Bluetooth chat services
              mChatService[0].start();
            }
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");
        final String TimeFormat = "%H:%M:%S - ";

        // Initialize the array adapter for the conversation thread
        mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
        mConversationView = (ListView) findViewById(R.id.in);
        mConversationView.setAdapter(mConversationArrayAdapter);


        // Initialize the send button with a listener that for click events
        mSendStartButton = (Button) findViewById(R.id.buttonSendStart);
        mSendStartButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                //TextView view = (TextView) findViewById(R.id.edit_text_out);
            	for (int i = 0; i< ConnectedDevices; i++){
            		get_log_num(ConnectedDevicesNames[i], 1, i);
            	}
            	now.setToNow();
            	mConversationArrayAdapter.add(now.format(TimeFormat) + "Sent Start Stream");
                String message = "= =";
                sendMessage(message);
            }
        });
        
        mSendStopButton = (Button) findViewById(R.id.buttonSendStop);
        mSendStopButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	int intTrialID;
            	intTrialID = Integer.parseInt(mTrialID.getText().toString());
            	intTrialID++;
            	mTrialID.setText(String.valueOf(intTrialID));
                // Send a message using content of the edit text widget
                //TextView view = (TextView) findViewById(R.id.edit_text_out);
            	now.setToNow();
                mConversationArrayAdapter.add(now.format(TimeFormat) +"Sent Stop");
                String message = ": :";
                sendMessage(message);
                
            }
        });

        mStartLogButton = (Button) findViewById(R.id.buttonStartLog);
        mStartLogButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	String ConfigTrialPatient;
            	int intTrialID,intPatientId;
            	try{
            		intTrialID = Integer.parseInt(mTrialID.getText().toString());
            		intPatientId = Integer.parseInt(mPatientID.getText().toString());
            	}
            	catch(NumberFormatException nfe){
            		intTrialID = 0;
            		intPatientId = 0;
            	}
            	ConfigTrialPatient = "% SET PATIENTID "+ intTrialID + " " + intPatientId + "\r\n";
                // Send a message using content of the edit text widget
                //TextView view = (TextView) findViewById(R.id.edit_text_out);
            	now.setToNow();
                mConversationArrayAdapter.add(now.format(TimeFormat) + "Sent Start Log");
                
                String message = ConfigTrialPatient + "  - -";
                sendMessage(message);
            }
        });
        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService[0] = new BluetoothService(this, mHandler0);
        mChatService[1] = new BluetoothService(this, mHandler1);
        mChatService[2] = new BluetoothService(this, mHandler2);
        mChatService[3] = new BluetoothService(this, mHandler3);
        mChatService[4] = new BluetoothService(this, mHandler4);
        mChatService[5] = new BluetoothService(this, mHandler5);
        mChatService[6] = new BluetoothService(this, mHandler6);

//        // Initialize the buffer for outgoing messages
//        mOutStringBuffer = new StringBuffer("");
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
    	int i;
        super.onDestroy();
        // Stop the Bluetooth chat services
        for (i= 0; i<7; i++){
        	if (mChatService[i] != null) mChatService[i].stop();
        }
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    }

    private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
        	int currState;
            byte[] send = message.getBytes();
            for (int i = 0; i < 7; i++){
            	currState = mChatService[i].getState();
        		if (currState ==  BluetoothService.STATE_CONNECTED){
        			mChatService[i].StartStremingTime = android.os.SystemClock.elapsedRealtime();
        			mChatService[i].write(send);
        		}
            }
            if (ConnectedDevices == 0){
                // Check that we're actually connected to any node
                Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener =
        new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            // If the action is a key-up event on the return key, send the message
            if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                String message = view.getText().toString();
                sendMessage(message);
            }
            if(D) Log.i(TAG, "END onEditorAction");
            return true;
        }
    };

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Closing CUPID for Mobile")
            .setMessage("If you exit  all connections will be lost. Are you sure?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                finish();    
            }

        })
        .setNegativeButton("No", null)
        .show();
    }

    
    
//    final Handler[] mHandlerArray;
//    
//    mHandlerArray[0] = new Handler() {
//    	
//    };
    
    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler0 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[0] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter0 = (TextView) findViewById(R.id.textCount0);
            	final TextView textLostPck0 = (TextView) findViewById(R.id.textLostPck0);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter0.setText(" "+ rcvData[0]);
            	SensorsData[0]= rcvData[0];
            	textLostPck0.setText(" "+ rcvData[9]);
            	 aprLevelsSeries.setModel(Arrays.asList(SensorsData), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                 aprLevelsPlot.redraw();
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };

    private final Handler mHandler1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[1] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter1 = (TextView) findViewById(R.id.textCount1);
            	final TextView textLostPck1 = (TextView) findViewById(R.id.textLostPck1);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter1.setText(" "+ rcvData[0]);
            	SensorsData[1]= rcvData[0];
            	textLostPck1.setText(" "+ rcvData[9]);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };

    private final Handler mHandler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[2] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter2 = (TextView) findViewById(R.id.textCount2);
            	final TextView textLostPck2 = (TextView) findViewById(R.id.textLostPck2);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter2.setText(" "+ rcvData[0]);
            	SensorsData[2]= rcvData[0];
            	textLostPck2.setText(" "+ rcvData[9]);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    private final Handler mHandler3 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[3] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter3 = (TextView) findViewById(R.id.textCount3);
            	final TextView textLostPck3 = (TextView) findViewById(R.id.textLostPck3);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter3.setText(" "+ rcvData[0]);
            	SensorsData[3]= rcvData[0];
            	textLostPck3.setText(" "+ rcvData[9]);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    private final Handler mHandler4 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[4] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter4 = (TextView) findViewById(R.id.textCount4);
            	final TextView textLostPck4 = (TextView) findViewById(R.id.textLostPck4);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter4.setText(" "+ rcvData[0]);
            	SensorsData[4]= rcvData[0];
            	textLostPck4.setText(" "+ rcvData[9]);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    private final Handler mHandler5 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[5] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter5 = (TextView) findViewById(R.id.textCount5);
            	final TextView textLostPck5 = (TextView) findViewById(R.id.textLostPck5);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter5.setText(" "+ rcvData[0]);
            	SensorsData[5]= rcvData[0];
            	textLostPck5.setText(" "+ rcvData[9]);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    private final Handler mHandler6 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                	ConnectedDevices++;
                    mTitle.setText(R.string.title_connected_to);
                    ConnectedDevicesNames[6] = mConnectedDeviceName;
                    for (int i=0; i<7; i++){
                    	mTitle.append(ConnectedDevicesNames[i] + " ");
                    }
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_DISCONNECTED:
                	//ConnectedDevices--;
                	break;
                case BluetoothService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                break;
            case MESSAGE_READ:
            	final TextView textCounter6 = (TextView) findViewById(R.id.textCount6);
            	final TextView textLostPck6 = (TextView) findViewById(R.id.textLostPck6);
            	int [] rcvData;
            	rcvData = (int[]) msg.obj;
            	
            	textCounter6.setText(" "+ rcvData[0]);
            	SensorsData[6]= rcvData[0];
            	textLostPck6.setText(" "+ rcvData[9]);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        if (resultCode != 0){
	        switch (requestCode) {
	        case REQUEST_CONNECT_DEVICE_INSECURE:
	            // When DeviceListActivity returns with a device to connect
	        	int currState;
	        	String address = data.getExtras()
	                    .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
	                // Get the BLuetoothDevice object
	            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
	        	for (int i=0; i<7; i++){
	        		currState = mChatService[i].getState();
	        		if (currState < 2){
	        			mChatService[i].connect(device, false);
	        			return;
	        		}
	        	}
	
	        case REQUEST_ENABLE_BT:
	            // When the request to enable Bluetooth returns
	            if (resultCode == Activity.RESULT_OK) {
	                // Bluetooth is now enabled, so set up a chat session
	                setupChat();
	            } else {
	                // User did not enable Bluetooth or an error occured
	                Log.d(TAG, "BT not enabled");
	                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
	                finish();
	            }
	        }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent = null;
        switch (item.getItemId()) {
        case R.id.insecure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
            return true;
        }
        return false;
    }
    
@SuppressLint("SdCardPath")
	public int get_log_num(String node_id, int l_num_update,int service_id){
		
		if (mChatService[service_id].getState() == BluetoothService.STATE_CONNECTED){
	    	root = Environment.getExternalStorageDirectory();
		 	TimeZone tz = TimeZone.getTimeZone("Europe/Rome");
	
			Calendar rightNow = Calendar.getInstance(tz);// .getInstance();
			dirName="";
			dirName2="CUPID_data/"+rightNow.get(Calendar.DAY_OF_MONTH)+"_"+ (rightNow.get(Calendar.MONTH) + 1) +"_"+ rightNow.get(Calendar.YEAR) +"/";
	    	
			try{
	    		    dirName = "/sdcard/"+dirName2;
	    			//dirName = Environment.getExternalStorageDirectory().getPath()+dirName2;
	    		    File newFile = new File(dirName);
	    		    newFile.mkdirs();
	
	    	} 
	    	catch(Exception e)
	    	{
	    		Toast.makeText(this, "Exception creating folder " + e, Toast.LENGTH_LONG).show();
	    	} 
	    	
			if (root.canRead()) {
	
			}
			if (root.canWrite()){
				
				file_name_log = "log_"+rightNow.get(Calendar.YEAR)+"_"+ rightNow.get(Calendar.MONTH) +"_"+rightNow.get(Calendar.DAY_OF_MONTH)+"-"
						+rightNow.get(Calendar.HOUR_OF_DAY)+"_"+rightNow.get(Calendar.MINUTE)+"-"+node_id +"-"+ service_id+ ".txt";	
				
				//log_file = new File(root,"log_data/"+file_name_log);
				log_file = new File(root,dirName2+file_name_log);
				
				
				try {
					log_file_wr = new FileWriter(log_file);
					out_log = new BufferedWriter(log_file_wr);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
					mChatService[service_id].out_log=out_log;
			}
		}
	
	    	return 1;
	 }

}
